package com.takehome.takehome;

import com.takehome.takehome.api.TakeHomeApi;
import com.takehome.takehome.api.model.AuthResponse;
import com.takehome.takehome.api.model.UserRequest;
import com.takehome.takehome.api.model.UserSession;
import com.takehome.takehome.presenter.SignInPresenter;
import com.takehome.takehome.utils.ConnectionManager;
import com.takehome.takehome.utils.PreferencesManager;
import com.takehome.takehome.view.signin.SignInView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import rx.Observable;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by Claudio on 21/11/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class SignInPresenterTest {

    @Mock
    TakeHomeApi api;

    @Mock
    PreferencesManager preferencesManager;

    @Mock
    ConnectionManager connectionManager;

    @Mock
    SignInView view;

    private SignInPresenter presenter;
    private String token;
    private String email;
    private String invalidEmail;
    private UserRequest userRequest;
    private AuthResponse authResponse;

    @Before
    public void setUp() {
        this.token="xyz123@";
        this.email = "xyz@gmail.com";
        this.invalidEmail = "123abc";
        this.userRequest = new UserRequest(email, "123");
        this.authResponse = new AuthResponse(new UserSession(token));
        presenter = new SignInPresenter(api, preferencesManager, connectionManager);
        presenter.setView(view);
    }

    @Test
    public void shouldOpenDetailActivityWhenHaveStoredToken() {
        when(preferencesManager.getSessionToken()).thenReturn(token);

        presenter.initialize();

        verify(view).openDetailActivity();
    }

    @Test
    public void shouldShowErrorWhenIsInvalidEmail() {
        presenter.validateEmail(invalidEmail);

        verify(view).showUsernameError();
        verify(view, never()).hideUsernameError();
    }

    @Test
    public void shouldHideErrorWhenEmailIsValid() {
        presenter.validateEmail(email);

        verify(view).hideUsernameError();
        verify(view, never()).showUsernameError();
    }

    @Test
    public void shouldErrorMessageWhenHaveNoConnection() {
        when(connectionManager.isConnected()).thenReturn(false);

        presenter.signIn(userRequest);

        verify(view).showMessage(R.string.without_connection_error);
        verify(view, never()).openDetailActivity();
    }

    @Test
    public void shouldOpenDetailWhenReturnIsCorrect() {
        when(connectionManager.isConnected()).thenReturn(true);
        when(api.signIn(userRequest)).thenReturn(Observable.just(authResponse));

        presenter.signIn(userRequest);

        verify(view).openDetailActivity();

    }
}
