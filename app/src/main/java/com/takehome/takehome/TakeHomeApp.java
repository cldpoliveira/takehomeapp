package com.takehome.takehome;

import android.app.Application;
import android.content.Context;

import com.takehome.takehome.di.DaggerTakeHomeComponent;
import com.takehome.takehome.di.NetworkModule;
import com.takehome.takehome.di.TakeHomeComponent;

/**
 * Created by Claudio on 19/11/2017.
 */

public class TakeHomeApp extends Application {

    private TakeHomeComponent component;

    public static TakeHomeApp get(Context context) {
        return (TakeHomeApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
    }

    private void initComponent() {
        component = DaggerTakeHomeComponent.builder()
                    .networkModule(new NetworkModule(this))
                    .build();
    }

    public TakeHomeComponent getComponent() {
        return component;
    }

}
