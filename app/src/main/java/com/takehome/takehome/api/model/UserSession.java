package com.takehome.takehome.api.model;

/**
 * Created by Claudio on 19/11/2017.
 */

public class UserSession {
    private String token;

    public UserSession(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
