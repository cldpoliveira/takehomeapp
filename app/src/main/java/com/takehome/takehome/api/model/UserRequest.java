package com.takehome.takehome.api.model;

/**
 * Created by Claudio on 19/11/2017.
 */

public class UserRequest {
    private String username;
    private String password;

    public UserRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
