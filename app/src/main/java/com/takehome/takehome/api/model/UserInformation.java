package com.takehome.takehome.api.model;

/**
 * Created by Claudio on 19/11/2017.
 */

public class UserInformation {
    private String token;
    private String username;
    private String firstName;
    private String lastName;
    private String password;

    public UserInformation(String token, String firstName, String lastName, String password) {
        this.token = token;
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        this.username = null;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if(firstName.isEmpty()) {
            this.firstName = null;
        } else {
            this.firstName = firstName;
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName.isEmpty()) {
            this.lastName = null;
        } else {
            this.lastName = lastName;
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if(password.isEmpty()) {
            this.password = null;
        } else {
            this.password = password;
        }
    }

    public boolean isValid() {
        return firstName != null || lastName != null || password != null;
    }
}
