package com.takehome.takehome.api;

import com.takehome.takehome.api.model.AuthResponse;
import com.takehome.takehome.api.model.UserInformation;
import com.takehome.takehome.api.model.UserRequest;
import com.takehome.takehome.api.model.UserResponse;
import com.takehome.takehome.api.model.UserSession;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Claudio on 19/11/2017.
 */

public interface TakeHomeApi {

    @POST("user/create")
    Observable<Void> createUser(@Body UserRequest user);

    @POST("auth/authUser")
    Observable<AuthResponse> signIn(@Body UserRequest user);

    @POST("user/getDetails")
    Observable<UserResponse> getDetails(@Body UserSession session);

    @POST("user/update")
    Observable<Void> userUpdate(@Body UserInformation userInformation);

}
