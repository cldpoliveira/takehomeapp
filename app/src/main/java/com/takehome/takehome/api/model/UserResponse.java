package com.takehome.takehome.api.model;

/**
 * Created by Claudio on 19/11/2017.
 */

public class UserResponse {
    private UserInformation user;

    public UserResponse(UserInformation user) {
        this.user = user;
    }

    public UserInformation getUser() {
        return user;
    }
}
