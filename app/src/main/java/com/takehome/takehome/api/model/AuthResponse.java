package com.takehome.takehome.api.model;

/**
 * Created by Claudio on 19/11/2017.
 */

public class AuthResponse {
    private UserSession session;

    public AuthResponse(UserSession session) {
        this.session = session;
    }

    public String getSession() {
        return session.getToken();
    }
}
