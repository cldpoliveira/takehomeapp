package com.takehome.takehome.view.newaccount;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.takehome.takehome.R;
import com.takehome.takehome.api.model.UserRequest;
import com.takehome.takehome.presenter.NewAccountPresenter;
import com.takehome.takehome.view.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class NewAccountActivity extends BaseActivity implements NewAccountView {

    @BindView(R.id.etUser)
    EditText etUser;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    NewAccountPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        setContentView(R.layout.activity_new_account);
        setTitle(getString(R.string.new_account_activity), true);
        presenter.setView(this);
        initFocusListener();
    }

    private void initFocusListener() {
        etUser.setOnFocusChangeListener((view, hasFocus) -> {
            if(!hasFocus) presenter.validateEmail(etUser.getText().toString());
        });

        etPassword.setOnFocusChangeListener((view, hasFocus) -> {
            if(!hasFocus) presenter.validatePassword(etPassword.getText().toString());
        });
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.animate();

        });
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> {
            progressBar.clearAnimation();
            progressBar.setVisibility(View.GONE);
        });
    }

    @Override
    public void backSignInActivity() {
        runOnUiThread(() -> finish());
    }

    @Override
    public void showEmailError() {
        runOnUiThread(() -> etUser.setError(getString(R.string.email_error)));
    }

    @Override
    public void hideEmailError() {
        runOnUiThread(() -> etUser.setError(null));
    }

    @Override
    public void showPasswordError() {
        runOnUiThread(() -> etPassword.setError(getString(R.string.password_error)));
    }

    @Override
    public void hidePasswordError() {
        runOnUiThread(() -> etPassword.setError(null));
    }

    @OnClick(R.id.btnNewAccount)
    public void onNewAccountClick() {
        UserRequest userRequest = new UserRequest(etUser.getText().toString(), etPassword.getText().toString());
        presenter.createNewAccount(userRequest);

    }
}
