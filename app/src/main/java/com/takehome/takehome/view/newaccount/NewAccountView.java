package com.takehome.takehome.view.newaccount;

import com.takehome.takehome.view.IView;

/**
 * Created by Claudio on 20/11/2017.
 */

public interface NewAccountView extends IView {
    void showProgress();
    void hideProgress();
    void backSignInActivity();
    void showEmailError();
    void hideEmailError();
    void showPasswordError();
    void hidePasswordError();

}
