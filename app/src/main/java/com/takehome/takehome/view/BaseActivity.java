package com.takehome.takehome.view;

import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.takehome.takehome.TakeHomeApp;
import com.takehome.takehome.di.TakeHomeComponent;

import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity implements IView {

    @Override
    public void showMessage(@StringRes int message) {
        runOnUiThread(() -> Toast.makeText(this, message, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public TakeHomeComponent getComponent() {
        return TakeHomeApp.get(this).getComponent();
    }

    public void setTitle(String title, boolean enableUpNavigation) {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(title);
            actionBar.setDisplayHomeAsUpEnabled(enableUpNavigation);
        }
    }

}
