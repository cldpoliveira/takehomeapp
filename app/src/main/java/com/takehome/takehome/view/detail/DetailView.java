package com.takehome.takehome.view.detail;

import com.takehome.takehome.api.model.UserInformation;
import com.takehome.takehome.view.IView;

/**
 * Created by Claudio on 21/11/2017.
 */

public interface DetailView extends IView {
    void showProgress();
    void hideProgress();
    void showErrorFirstName();
    void hideErrorFirstName();
    void showErrorLastName();
    void hideErrorLastName();
    void showPasswordError();
    void hidePasswordError();
    void doLoginAgain();
    void setFields(UserInformation userInformation);
}
