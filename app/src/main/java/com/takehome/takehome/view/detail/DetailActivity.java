package com.takehome.takehome.view.detail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.takehome.takehome.R;
import com.takehome.takehome.api.model.UserInformation;
import com.takehome.takehome.presenter.DetailPresenter;
import com.takehome.takehome.view.BaseActivity;
import com.takehome.takehome.view.signin.SignInActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class DetailActivity extends BaseActivity implements DetailView {

    @BindView(R.id.etFirstName)
    EditText etFirstName;

    @BindView(R.id.etLastName)
    EditText etLastName;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.etUsername)
    EditText etUsername;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    DetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        setContentView(R.layout.activity_detail);
        setTitle(getString(R.string.detail_activity), true);
        presenter.setView(this);
        presenter.getUserDetails();
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.animate();
        });
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> {
            progressBar.clearAnimation();
            progressBar.setVisibility(View.GONE);
        });
    }

    @Override
    public void showErrorFirstName() {
        runOnUiThread(() -> etFirstName.setError(getString(R.string.name_error)));
    }

    @Override
    public void hideErrorFirstName() {
        runOnUiThread(() -> etFirstName.setError(null));
    }

    @Override
    public void showErrorLastName() {
        runOnUiThread(() -> etLastName.setError(getString(R.string.name_error)));
    }

    @Override
    public void hideErrorLastName() {
        runOnUiThread(() -> etLastName.setError(null));
    }

    @Override
    public void showPasswordError() {
        runOnUiThread(() -> etPassword.setError(getString(R.string.password_error)));
    }

    @Override
    public void hidePasswordError() {
        runOnUiThread(() -> etPassword.setError(null));
    }

    @Override
    public void doLoginAgain() {
        runOnUiThread(() -> {
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public void setFields(UserInformation userInformation) {
        runOnUiThread(() -> {
            etUsername.setText(userInformation.getUserName());
            etFirstName.setText(userInformation.getFirstName());
            etLastName.setText(userInformation.getLastName());
        });
    }

    @OnClick(R.id.btnEdit)
    public void onEditClick() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String password = etPassword.getText().toString();

        presenter.updateUser(firstName, lastName, password);
    }

    @OnClick(R.id.btnLogoff)
    public void onLogoffClick() {
        presenter.logoff();
    }
}
