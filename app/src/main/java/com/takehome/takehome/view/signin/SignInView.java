package com.takehome.takehome.view.signin;

import com.takehome.takehome.view.IView;

/**
 * Created by Claudio on 20/11/2017.
 */

public interface SignInView extends IView {
    void showProgress();
    void hideProgress();
    void openDetailActivity();
    void showUsernameError();
    void hideUsernameError();
    void showPasswordError();
    void hidePasswordError();
}
