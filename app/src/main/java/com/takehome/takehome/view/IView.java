package com.takehome.takehome.view;

import android.support.annotation.StringRes;

/**
 * Created by Claudio on 20/11/2017.
 */

public interface IView {
    void showMessage(@StringRes int message);
}
