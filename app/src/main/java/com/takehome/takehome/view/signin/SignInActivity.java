package com.takehome.takehome.view.signin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.takehome.takehome.R;
import com.takehome.takehome.api.model.UserRequest;
import com.takehome.takehome.presenter.SignInPresenter;
import com.takehome.takehome.view.BaseActivity;
import com.takehome.takehome.view.detail.DetailActivity;
import com.takehome.takehome.view.newaccount.NewAccountActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements SignInView {

    @BindView(R.id.etUser)
    EditText etUser;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    SignInPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        setContentView(R.layout.activity_sign_in);
        setTitle(getString(R.string.login_activity), false);
        presenter.setView(this);
        initFocusListener();
        presenter.initialize();
    }

    private void initFocusListener() {
        etUser.setOnFocusChangeListener((view, hasFocus) -> {
            if(!hasFocus) presenter.validateEmail(etUser.getText().toString());
        });

        etPassword.setOnFocusChangeListener((view, hasFocus) -> {
            if(!hasFocus) presenter.validatePassword(etPassword.getText().toString());
        });
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.animate();
        });
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> {
            progressBar.clearAnimation();
            progressBar.setVisibility(View.GONE);
        });
    }

    @Override
    public void openDetailActivity() {
        runOnUiThread(() -> {
            Intent intent = new Intent(this, DetailActivity.class);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public void showUsernameError() {
        runOnUiThread(() -> etUser.setError(getString(R.string.email_error)));

    }

    @Override
    public void hideUsernameError() {
        runOnUiThread(() -> etUser.setError(null));
    }

    @Override
    public void showPasswordError() {
        runOnUiThread(() -> etPassword.setError(getString(R.string.password_error)));
    }

    @Override
    public void hidePasswordError() {
        runOnUiThread(() -> etPassword.setError(null));
    }

    @OnClick(R.id.btnLogin)
    public void onClickLogin() {
        UserRequest userRequest = new UserRequest(etUser.getText().toString(), etPassword.getText().toString());
        presenter.signIn(userRequest);
    }

    @OnClick(R.id.txtNewAccount)
    public void onClickNewAccount() {
        Intent intent = new Intent(this, NewAccountActivity.class);
        startActivity(intent);
    }
}
