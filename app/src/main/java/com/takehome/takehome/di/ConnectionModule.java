package com.takehome.takehome.di;

import android.content.Context;

import com.takehome.takehome.utils.ConnectionManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Claudio on 20/11/2017.
 */

@Module
public class ConnectionModule {

    @Provides
    @Singleton
    public ConnectionManager provideConnectionManager(Context context) {
        return new ConnectionManager(context);
    }
}
