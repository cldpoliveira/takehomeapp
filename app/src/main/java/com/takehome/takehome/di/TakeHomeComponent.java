package com.takehome.takehome.di;

import com.takehome.takehome.view.detail.DetailActivity;
import com.takehome.takehome.view.newaccount.NewAccountActivity;
import com.takehome.takehome.view.signin.SignInActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Claudio on 19/11/2017.
 */

@Singleton
@Component(modules = {NetworkModule.class, DatabaseModule.class, ConnectionModule.class})
public interface TakeHomeComponent {

    void inject(SignInActivity signInActivity);

    void inject(NewAccountActivity newAccountActivity);

    void inject(DetailActivity detailActivity);
}
