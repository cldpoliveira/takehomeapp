package com.takehome.takehome.di;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.takehome.takehome.api.TakeHomeApi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by Claudio on 19/11/2017.
 */

@Module
public class NetworkModule {

    private static final int TIMEOUT_VALUE = 10;
    private Context context;

    public NetworkModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    public HttpUrl provideHttpUrl() {
        return HttpUrl.parse("http://104.131.189.211:8085/");
    }

    @Provides
    @Singleton
    public TakeHomeApi provideTakeHomeApi(Retrofit retrofit) {
        return retrofit.create(TakeHomeApi.class);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_VALUE, TimeUnit.SECONDS);

        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(HttpUrl baseUrl, OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

}
