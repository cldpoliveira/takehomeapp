package com.takehome.takehome.di;

import android.content.Context;

import com.takehome.takehome.utils.PreferencesManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Claudio on 20/11/2017.
 */

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    public PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
