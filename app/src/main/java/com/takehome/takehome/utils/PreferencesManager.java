package com.takehome.takehome.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

/**
 * Created by Claudio on 20/11/2017.
 */

public class PreferencesManager {
    private Context context;

    private final String SHARED_PREF_ID = "shared_pref_id";
    private final String KEY = "session_token";
    private final String DEFAULT_VALUE = "";

    public PreferencesManager(Context context) {
        this.context = context;
    }

    public void setSessionToken(@NonNull String sessionToken) {
        SharedPreferences sp = context.getSharedPreferences(SHARED_PREF_ID, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY, sessionToken);
        editor.apply();
    }

    public String getSessionToken() {
        SharedPreferences sp = context.getSharedPreferences(SHARED_PREF_ID, 0);
        return sp.getString(KEY, DEFAULT_VALUE);
    }

    public void cleanSessionToken() {
        SharedPreferences sp = context.getSharedPreferences(SHARED_PREF_ID, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(KEY);
        editor.apply();
    }
}
