package com.takehome.takehome.utils;

/**
 * Created by Claudio on 20/11/2017.
 */

public class ValidationUtils {
    private static final String NAME = "[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]{3,80}";
    private static final String PASSWORD = "[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ0-9 ]{3,16}";
    private static final String EMAIL = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";

    public static boolean isValidName(String name) {
        return name.matches(NAME);
    }

    public static boolean isValidPassword(String password) {
        return password.matches(PASSWORD);
    }

    public static boolean isValidEmail(String email) {
        return email.matches(EMAIL);
    }
}
