package com.takehome.takehome.presenter;

import com.takehome.takehome.R;
import com.takehome.takehome.api.TakeHomeApi;
import com.takehome.takehome.api.model.UserInformation;
import com.takehome.takehome.api.model.UserSession;
import com.takehome.takehome.utils.ConnectionManager;
import com.takehome.takehome.utils.PreferencesManager;
import com.takehome.takehome.utils.ValidationUtils;
import com.takehome.takehome.view.detail.DetailView;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.schedulers.Schedulers;

/**
 * Created by Claudio on 21/11/2017.
 */

@Singleton
public class DetailPresenter {
    private TakeHomeApi api;
    private ConnectionManager connectionManager;
    private PreferencesManager preferencesManager;
    private DetailView view;

    @Inject
    public DetailPresenter(TakeHomeApi api, ConnectionManager connectionManager, PreferencesManager preferencesManager) {
        this.api = api;
        this.connectionManager = connectionManager;
        this.preferencesManager = preferencesManager;
    }

    public void setView(DetailView view) {
        this.view = view;
    }

    public boolean validateFirstName(String name) {
        boolean isValidFirstName = ValidationUtils.isValidName(name) || name.isEmpty();

        if(isValidFirstName) {
            view.hideErrorFirstName();
        } else {
            view.showErrorFirstName();
        }

        return isValidFirstName;
    }

    public boolean validateLastName(String lastName) {
        boolean isValidLastName = ValidationUtils.isValidName(lastName) || lastName.isEmpty();

        if(isValidLastName) {
            view.hideErrorLastName();
        } else {
            view.showErrorLastName();
        }

        return isValidLastName;
    }

    public boolean validatePassword(String password) {
        boolean isValidPassword = ValidationUtils.isValidPassword(password) || password.isEmpty();

        if(isValidPassword) {
            view.hidePasswordError();
        } else {
            view.showPasswordError();
        }

        return isValidPassword;
    }

    public void logoff() {
        preferencesManager.cleanSessionToken();
        view.doLoginAgain();
    }

    public void getUserDetails() {
        if (connectionManager.isConnected()) {
            callUserDetails();
        } else {
            view.showMessage(R.string.without_connection_error);
        }
    }

    private void callUserDetails() {
        String token = preferencesManager.getSessionToken();
        api.getDetails(new UserSession(token))
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(() -> view.showProgress())
                .doOnError(error -> view.hideProgress())
                .subscribe(userResponse -> {
                    view.hideProgress();
                    view.setFields(userResponse.getUser());
                }, error -> {
                    view.showMessage(R.string.invalid_token);
                    preferencesManager.cleanSessionToken();
                    view.doLoginAgain();
                });
    }

    public void updateUser(String name, String lastName, String password) {
        String token = preferencesManager.getSessionToken();
        UserInformation userInformation = new UserInformation(token, name, lastName, password);

        if(validateLastName(lastName) && validateFirstName(name) && validatePassword(password) && userInformation.isValid()) {
            if(connectionManager.isConnected()) {
                callUpdateUser(userInformation);
            } else {
                view.showMessage(R.string.without_connection_error);
            }
        } else {
            view.showMessage(R.string.field_empty);
        }
    }

    private void callUpdateUser(final UserInformation userInformation) {
        api.userUpdate(userInformation)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(() -> view.showProgress())
                .doOnError(error -> view.hideProgress())
                .subscribe(v -> {
                    view.hideProgress();
                    view.showMessage(R.string.update_user_successfully);
                    if(userInformation.getPassword() != null) {
                        view.showMessage(R.string.invalid_token);
                        preferencesManager.cleanSessionToken();
                        view.doLoginAgain();
                    }
                }, error -> {
                    view.showMessage(R.string.invalid_token);
                    view.doLoginAgain();
                });
    }

}
