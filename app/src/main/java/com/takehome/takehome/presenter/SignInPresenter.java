package com.takehome.takehome.presenter;

import com.takehome.takehome.R;
import com.takehome.takehome.api.TakeHomeApi;
import com.takehome.takehome.api.model.UserRequest;
import com.takehome.takehome.utils.ConnectionManager;
import com.takehome.takehome.utils.PreferencesManager;
import com.takehome.takehome.utils.ValidationUtils;
import com.takehome.takehome.view.signin.SignInView;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.schedulers.Schedulers;

/**
 * Created by Claudio on 20/11/2017.
 */

@Singleton
public class SignInPresenter {
    private TakeHomeApi api;
    private PreferencesManager preferencesManager;
    private ConnectionManager connectionManager;
    private SignInView view;

    @Inject
    public SignInPresenter(TakeHomeApi api, PreferencesManager preferencesManager, ConnectionManager connectionManager) {
        this.api = api;
        this.preferencesManager = preferencesManager;
        this.connectionManager = connectionManager;
    }

    public void setView(SignInView view) {
        this.view = view;
    }

    public void initialize() {
        String token = preferencesManager.getSessionToken();

        if (!token.isEmpty()) {
            view.openDetailActivity();
        }
    }

    public boolean validateEmail(String email) {
        boolean isValidEmail = ValidationUtils.isValidEmail(email);
        if (isValidEmail) {
            view.hideUsernameError();
        } else {
            view.showUsernameError();
        }

        return isValidEmail;
    }

    public boolean validatePassword(String password) {
        boolean isValidPassword = ValidationUtils.isValidPassword(password);
        if (isValidPassword) {
            view.hidePasswordError();
        } else {
            view.showPasswordError();
        }

        return isValidPassword;
    }

    public void signIn(UserRequest user) {
        if (validateEmail(user.getUsername()) && validatePassword(user.getPassword())) {
            if (connectionManager.isConnected()) {
                callSignIn(user);
            } else {
                view.showMessage(R.string.without_connection_error);
            }
        }
    }

    private void callSignIn(UserRequest user) {
        api.signIn(user)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(() -> view.showProgress())
                .doOnError(error -> view.hideProgress())
                .subscribe(authResponse -> {
                    view.hideProgress();
                    preferencesManager.setSessionToken(authResponse.getSession());
                    view.openDetailActivity();
                }, error -> view.showMessage(R.string.error_login));
    }

}
