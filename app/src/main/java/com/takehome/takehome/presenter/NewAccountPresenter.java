package com.takehome.takehome.presenter;

import com.takehome.takehome.R;
import com.takehome.takehome.api.TakeHomeApi;
import com.takehome.takehome.api.model.UserRequest;
import com.takehome.takehome.utils.ConnectionManager;
import com.takehome.takehome.utils.ValidationUtils;
import com.takehome.takehome.view.newaccount.NewAccountView;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.schedulers.Schedulers;

/**
 * Created by Claudio on 20/11/2017.
 */

@Singleton
public class NewAccountPresenter {
    private TakeHomeApi api;
    private ConnectionManager connectionManager;
    private NewAccountView view;

    @Inject
    public NewAccountPresenter(TakeHomeApi api, ConnectionManager connectionManager) {
        this.api = api;
        this.connectionManager = connectionManager;
    }

    public void setView(NewAccountView view) {
        this.view = view;
    }

    public boolean validateEmail(String email) {
        boolean isValidEmail = ValidationUtils.isValidEmail(email);
        if (isValidEmail) {
            view.hideEmailError();
        } else {
            view.showEmailError();
        }

        return isValidEmail;
    }

    public boolean validatePassword(String password) {
        boolean isValidPassword = ValidationUtils.isValidPassword(password);
        if (isValidPassword) {
            view.hidePasswordError();
        } else {
            view.showPasswordError();
        }

        return isValidPassword;
    }

    public void createNewAccount(UserRequest user) {
        if (validateEmail(user.getUsername()) && validatePassword(user.getPassword())) {
            if (connectionManager.isConnected()) {
                callCreateAccount(user);
            } else {
                view.showMessage(R.string.without_connection_error);
            }
        }
    }

    private void callCreateAccount(UserRequest user) {
        api.createUser(user)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(() -> view.showProgress())
                .doOnError(error -> view.hideProgress())
                .subscribe(authResponse -> {
                    view.hideProgress();
                    view.showMessage(R.string.new_user_successfully);
                    view.backSignInActivity();
                }, error -> view.showMessage(R.string.new_user_error));
    }

}
